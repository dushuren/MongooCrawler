package com.trytech.mongoocrawler.parser;

import com.trytech.mongoocrawler.client.common.http.WebResult;
import com.trytech.mongoocrawler.client.common.queue.FetcherEvent;
import com.trytech.mongoocrawler.client.common.queue.FetcherWorkHandler;
import com.trytech.mongoocrawler.pipeline.item.LianjiaItem;

/**
 * 解析链家二手房网页，提取关键信息
 */
public class LianJiaPageParser extends FetcherWorkHandler implements Parser<LianjiaItem>{
    @Override
    public void onEvent(FetcherEvent event) throws Exception {
        WebResult<String> result = event.getWebResult();
        if(result == null) throw new NullPointerException("get a null result from queue in disruptor,@see com.trytech.mongoocrawler.parser.onEvent(FetcherEvent event)");
    }

    @Override
    public LianjiaItem parse(String html) {

        return null;
    }
}
