package com.trytech.mongoocrawler.pipeline.store;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * <code>MySqlStore</code>是mysql数据源的实体对象
 * @author coliza@sina.com
 * @date 2016年11月25日
 */
public abstract class MySqlStore implements Storable{
    private Map<String,String> druidMap;
    //数据库连接url
    protected String url;
    //数据库连接用户名
    protected String user;
    //数据库密码
    protected String password;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*****************
     * 初始化druid获取数据源
     *****************/
    protected DataSource getDataSource() throws Exception {
        return DruidDataSourceFactory.createDataSource(getDuridConfig());
    }

    protected Map<String,String> getDuridConfig(){
        if(druidMap == null) {
            druidMap = new HashMap<>();
        }
        druidMap.put(DruidDataSourceFactory.PROP_URL, getUrl());
        druidMap.put(DruidDataSourceFactory.PROP_USERNAME, getUser());
        druidMap.put(DruidDataSourceFactory.PROP_PASSWORD, getPassword());
        return druidMap;
    }
}
