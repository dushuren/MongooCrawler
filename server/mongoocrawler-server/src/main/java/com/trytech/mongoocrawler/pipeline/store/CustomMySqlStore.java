package com.trytech.mongoocrawler.pipeline.store;

/**
 * <code>CustomMySqlStore</code>是使用mysql存储对象的一个范例
 * @author coliza@sina.com
 * @date 2016年11月25日
 */
public class CustomMySqlStore extends MySqlStore{
    @Override
    public void store() throws Exception {
        getDataSource();
    }
}
