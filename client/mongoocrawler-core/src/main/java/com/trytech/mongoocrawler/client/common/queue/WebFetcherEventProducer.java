package com.trytech.mongoocrawler.client.common.queue;

import com.lmax.disruptor.RingBuffer;
import com.trytech.mongoocrawler.client.common.http.WebResult;

/**
 * Created by hp on 2017-1-25.
 */
public class WebFetcherEventProducer extends FetcherEventProducer<WebResultFetcherEvent,WebResult<String>> {
    public WebFetcherEventProducer(RingBuffer<WebResultFetcherEvent> ringBuffer) {
        super(ringBuffer);
    }
}
