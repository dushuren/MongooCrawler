package com.trytech.mongoocrawler.client.common.queue;

/**
 * Created by hp on 2017-1-25.
 */
public class WebFetcherEventFactory extends FetcherEventFactory<WebResultFetcherEvent> {
    @Override
    public WebResultFetcherEvent newInstance() {
        return new WebResultFetcherEvent();
    }
}
