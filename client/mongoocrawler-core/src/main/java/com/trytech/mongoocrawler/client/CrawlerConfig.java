package com.trytech.mongoocrawler.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by hp on 2017-2-17.
 */
public class CrawlerConfig {
    private final static String CRAWLER_URL_FETCH_TIMEOUT = "crawler.url.fetch.timeout";
    private final static String CRAWLER_RUN_MODE = "crawler.run.mode";
    private final static String CRAWLER_START_URLS = "crawler.start_urls";
    private final static String CRAWLER_REDIS_IP = "crawler.redis.ip";
    private final static String CRAWLER_REDIS_PORT = "crawler.redis.port";
    private final static String CRAWLER_URL_STORE_MODE = "crawler.url_store_mode";
    private static ConcurrentHashMap<String,Object> properties = new ConcurrentHashMap<String,Object>();

    public static enum URL_STORE_MODE{
        LOCAL("LOCAL"),REDIS("REDIS");
        private String value;
        URL_STORE_MODE(String value){
            this.value = value;
        }
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        public boolean equals(URL_STORE_MODE mode){
            return this.value.equals(mode.getValue());
        }
    }
    public static CrawlerConfig newInstance(String path) throws IOException {
        Properties p = new Properties();
        FileInputStream fileStream  = new FileInputStream(path);
        p.load(fileStream);
        if(p.containsKey(CRAWLER_REDIS_IP)){
            properties.put(CRAWLER_REDIS_IP,p.getProperty(CRAWLER_REDIS_IP,null));
        }
        if(p.containsKey(CRAWLER_REDIS_PORT)){
            properties.put(CRAWLER_REDIS_PORT, p.getProperty(CRAWLER_REDIS_PORT, null));
        }
        if(p.containsKey(CRAWLER_START_URLS)){
            properties.put(CRAWLER_START_URLS, p.getProperty(CRAWLER_START_URLS,null));
        }
        if(p.containsKey(CRAWLER_RUN_MODE)){
            properties.put(CRAWLER_RUN_MODE,p.getProperty(CRAWLER_RUN_MODE,null));
        }
        if(p.containsKey(CRAWLER_URL_FETCH_TIMEOUT)){
            properties.put(CRAWLER_URL_FETCH_TIMEOUT,p.getProperty(CRAWLER_URL_FETCH_TIMEOUT,null));
        }
        if(p.containsKey(CRAWLER_URL_STORE_MODE)){
            properties.put(CRAWLER_URL_STORE_MODE, p.getProperty(CRAWLER_URL_STORE_MODE,"LOCAL"));
        }
        CrawlerConfig config = new CrawlerConfig();
        config.setProperties(properties);
        return config;
    }

    private void setProperties(ConcurrentHashMap p){
        properties = p;
    }

    /****************************
     * 获取运行模式
     ***************************/
    public Integer getRunMode(){
        return getInt(CRAWLER_RUN_MODE);
    }
    /****************************
     * 获取取url的超时时间
     ***************************/
    public Integer getFetchTimeout(){
        return getInt(CRAWLER_URL_FETCH_TIMEOUT);
    }
    /****************************
     * 获取起始url
     * @return
     ****************************/
    public String[] getStartUrls(){
        String start_url_str = getString(CRAWLER_START_URLS);
        if(start_url_str == null)return null;
        return start_url_str.split(";");
    }

    /**************************
     * 获取redis ip
     * @return
     **************************/
    public String getRedisIp(){
        return getString(CRAWLER_REDIS_IP);
    }
    /**************************
     * 获取redis端口
     * @return 端口
     **************************/
    public int getRedisPort(){
        return getInt(CRAWLER_REDIS_PORT);
    }
    /**********************************
     * url存储在哪里
     **********************************/
    public URL_STORE_MODE getUrlStoreMode(){
        if(URL_STORE_MODE.REDIS.equals(getString(CRAWLER_URL_STORE_MODE))){
            return URL_STORE_MODE.REDIS;
        }
        return URL_STORE_MODE.LOCAL;
    }
    private String getString(String key){
        Object obj = properties.get(key);
        if(obj != null){
            return obj.toString();
        }
        return null;
    }
    private Integer getInt(String key){
        Object obj = properties.get(key);
        if(obj==null)return null;
        return Integer.parseInt(obj.toString());
    }
}
