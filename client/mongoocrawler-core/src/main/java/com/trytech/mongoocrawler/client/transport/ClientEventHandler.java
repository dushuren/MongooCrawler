package com.trytech.mongoocrawler.client.transport;

import io.netty.buffer.ChannelBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * Created by coliza on 2016/11/11.
 */
public class ClientEventHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void inboundBufferUpdated(ChannelHandlerContext channelHandlerContext) throws Exception {

    }

    @Override
    public ChannelBuf newInboundBuffer(ChannelHandlerContext channelHandlerContext) throws Exception {
        return null;
    }

    @Override
    public void freeInboundBuffer(ChannelHandlerContext channelHandlerContext, ChannelBuf channelBuf) throws Exception {

    }
}
