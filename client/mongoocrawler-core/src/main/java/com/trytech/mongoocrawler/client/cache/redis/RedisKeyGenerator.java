package com.trytech.mongoocrawler.client.cache.redis;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Redis 唯一key的创建器
 * @author Collin Chiang
 */
public class RedisKeyGenerator {
    private static String KEY_PREFIX = "MONGOOCRAWLER_URLQUEUE_";
    public static String getKey(){
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        return KEY_PREFIX+format.format(now);
    }
}
