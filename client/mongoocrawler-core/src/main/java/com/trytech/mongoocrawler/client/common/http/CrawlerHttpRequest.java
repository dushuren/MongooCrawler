package com.trytech.mongoocrawler.client.common.http;

import com.trytech.mongoocrawler.client.parser.HtmlParser;

import java.net.URL;
import java.util.Map;

/**
 * Created by coliza on 2016/11/3.
 */
public class CrawlerHttpRequest {
    private URL url;
    private Map<String,Object> params;
    private HtmlParser parser;

    public CrawlerHttpRequest(URL url, Map<String,Object> params,HtmlParser parser) {
        this.url = url;
        this.params = params;
        this.parser = parser;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public HtmlParser getParser() {
        return parser;
    }

    public void setParser(HtmlParser parser) {
        this.parser = parser;
    }
}
