package com.trytech.mongoocrawler.client.transport;

import com.trytech.mongoocrawler.client.cache.redis.CrawlerRedisSession;
import com.trytech.mongoocrawler.client.cache.redis.RedisClient;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by coliza on 2017/4/9.
 */
public class RedisUrlManager extends UrlManager {
    //redis客户端
    private RedisClient client;
    //运行环境
    private CrawlerRedisSession container;

    public RedisUrlManager(CrawlerRedisSession container, RedisClient client){
        this.client = client;
        this.container = container;
    }
    public CrawlerRedisSession getContainer(){
        return container;
    }
    @Override
    public void pushUrl(String url) {
        client.insertToQueue(container.getRedisKey(),url);
    }

    @Override
    public void pushUrls(String[] urls) {
        for(String url:urls){
            pushUrl(url);
        }
    }
    @Override
    public URL popUrl() {
        try {
            return new URL(client.fetchFromQueue(container.getRedisKey()));
        }catch (MalformedURLException e){
            return null;
        }
    }
}
