package com.trytech.mongoocrawler.client.common.util;

import com.trytech.mongoocrawler.client.common.http.CrawlHttpUrlParam;
import org.apache.http.NameValuePair;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by hp on 2017-1-24.
 */
public class HttpUtils {
    public static List<? extends NameValuePair> paramFromMap(Map<String, Object> params){
        List paramList = new LinkedList();
        if(params != null) {
            for (String key : params.keySet()) {
                CrawlHttpUrlParam param = new CrawlHttpUrlParam(key, (String) params.get(key));
                paramList.add(param);
            }
        }
        return paramList;
    }
}
